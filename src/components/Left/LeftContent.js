import React, { Component } from "react";
import styled from "styled-components";

import iconBank from "../../img/group-15.png";
import UserInfo from "./UserInfo";
import NavBar from "./NavBar";

const StyledNavBar = styled.div`
  min-width: 260px;
  background-image: linear-gradient(121deg, #00a1e4, #005d98);

  & .wrap-img {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 60px;
    background: #1d81ac;
  }

  & .wrap-img img {
    width: 146px;
    height: 30px;
  }
`;

class LeftContent extends Component {
  render() {
    return (
      <StyledNavBar>
        <div className="wrap-img">
          <img src={iconBank} alt="icon-bank" />
        </div>
        <UserInfo />
        <NavBar />
      </StyledNavBar>
    );
  }
}

export default LeftContent;
