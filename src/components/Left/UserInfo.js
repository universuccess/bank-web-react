import React, { Component } from "react";
import styled from "styled-components";

const StyledUserInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 282px;

  & > img {
    margin-top: 43px;
    width: 120px;
    height: 120px;
    border-radius: 50%;
  }

  & > .user-name {
    margin-top: 17px;
    font-family: "Roboto";
    font-size: 18px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }

  & > .last-login {
    margin-top: 4px;
    font-family: Roboto;
    font-size: 12px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
  }
`;

class UserInfo extends Component {
  render() {
    return (
      <StyledUserInfo>
        <img src="https://picsum.photos/200/300" alt="img-user" />
        <div className="user-name">Nguyễn Hoàng Yến</div>
        <div className="last-login">Last login at 12:02 PM - Oct 01, 2018</div>
      </StyledUserInfo>
    );
  }
}

export default UserInfo;
