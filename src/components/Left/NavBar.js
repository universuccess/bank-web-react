import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import weeIcon from "../../img/group-12.png";

const StyledNavBar = styled.div`
  display: flex;
  flex-direction: column;
`;

const ItemNavBar = styled.div`
  height: 50px;
  display: flex;
  align-items: center;
  box-shadow: inset 0 0.5px 0 0 rgba(255, 255, 255, 0.5)
    ${props => (props.shadowBottom ? ", " + props.shadowBottom : "")};

  &:hover {
    cursor: pointer;
    background: rgba(0, 139, 204, 0.8);
    border-right: 6px solid #f9890c;
  }

  &:visited {
    background: rgba(255, 255, 255, 0.5);
  }

  &.send-or-request {
    justify-content: center;
    opacity: 0.8;
    font-family: Roboto;
    font-size: 12px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
    background-image: linear-gradient(80deg, #9000ff, #005cec, #005bec);
  }

  & i {
    margin: 17px;
    color: #ffffff;
  }

  & .title-item-nav {
    font-family: Roboto;
    font-size: 14px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
    flex-grow: 1;
  }

  & .poin-red {
    width: 14px;
    height: 14px;
    border-radius: 50%;
    background: #e23030;
    margin-right: 23px;
  }
`;

class NavBar extends Component {
  render() {
    return (
      <StyledNavBar>
        <Link to="/">
          <ItemNavBar>
            <i className="fas fa-home" />
            <span className="title-item-nav"> Home</span>
          </ItemNavBar>
        </Link>
        <ItemNavBar>
          <i className="fas fa-money-check-alt" />
          <span className="title-item-nav">Money Transfer</span>
        </ItemNavBar>
        <ItemNavBar>
          <i className="fas fa-money-bill-wave" />
          <span className="title-item-nav">Home</span>
        </ItemNavBar>
        <ItemNavBar className="send-or-request">
          Send or Request Money with <img src={weeIcon} alt="wee-icon" />
        </ItemNavBar>
        <ItemNavBar>
          <i className="fas fa-money-bill-wave" />
          <span className="title-item-nav">Payment</span>
        </ItemNavBar>
        <ItemNavBar>
          <i className="fas fa-cube" />
          <span className="title-item-nav">Product Services</span>
        </ItemNavBar>
        <Link to="/chat">
          <ItemNavBar shadowBottom="inset 0 -0.5px 0 0 rgba(255, 255, 255, 0.5)">
            <i className="fab fa-rocketchat" />
            <span className="title-item-nav">Chat</span>
            <span className="poin-red" />
          </ItemNavBar>
        </Link>
      </StyledNavBar>
    );
  }
}

export default NavBar;
