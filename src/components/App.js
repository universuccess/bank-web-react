import React, { Component } from "react";
import { createGlobalStyle } from "styled-components";
import { Route } from "react-router-dom";

import LeftContent from "./Left/LeftContent";
import RightContent from "./Right/RightContent";

const GlobalStyle = createGlobalStyle` 


  html {
    box-sizing: border-box;
  }

  body {
    height: 100vh;
  }

  *,*:before, *::after {
    box-sizing: inherit;
  }

  #root {
    display:flex;
    flex-direction: row;
    align-items: stretch;
  }


`;

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <GlobalStyle />
        <LeftContent />
        <RightContent />
      </React.Fragment>
    );
  }
}

export default App;
