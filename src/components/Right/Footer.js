import React, { Component } from "react";
import styled from "styled-components";

const StyledFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  min-width: 1100px;
  height: 79px;
  color: #ffffff;
  font-family: "Roboto";
  font-size: 12px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.67;
  letter-spacing: normal;
`;

export default class Footer extends Component {
  render() {
    return (
      <StyledFooter>
        <div className="col">
          <div className="name-bank">VIETNAM EXPORT IMPORT BANK</div>
          <div className="address-bank">
            8th Floor - Vincom Center, 72 Le Thanh Ton and 45A Ly Tu Trong
            Street, District 1, HCMC
          </div>
        </div>
        <div className="col">
          <div className="email-bank">ebanking@eximbank.com.vn</div>
          <div className="phone-bank">1800 1199</div>
        </div>
        <div className="col">Copyright &copy; Eximbank All Rights Reserved</div>
      </StyledFooter>
    );
  }
}
