import React, { Component } from "react";
import styled from "styled-components";

const StyledHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: #258fc5;
  height: 60px;
  color: #ffffff;

  & > .title-page {
    width: 225px;
    height: 100%;
    background: #70c0e4;
    display: flex;
    justify-content: center;
    align-items: center;
    clip-path: polygon(0% 0%, 90% 0%, 100% 50%, 90% 100%, 0% 100%);
  }

  & > .title-page > i {
    margin-right: 25.7px;
  }

  & > .title-page-text {
    font-family: "Roboto";
    font-size: 16px;
    font-weight: 400;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
  }

  & > .customer-services,
  & > .message,
  & > .language {
    font-size: 12px;
  }

  & > .customer-services {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    flex-grow: 1;
    padding-right: 20px;

    font-family: "Roboto";
    font-size: 12px;
    font-weight: 300;
  }

  & > .customer-services > i {
    width: 16px;
    height: 16px;
    border-radius: 3px;
    margin-right: 10px;
  }

  .message,
  .language {
    width: 100px;
    text-align: center;
  }

  .message > i {
    position: relative;
  }

  .message > i > .number-message {
    width: 14px;
    height: 14px;
    border-radius: 50%;
    background: #e23030;
    position: absolute;
    top: 50%;
    left: 50%;

    font-family: Roboto;
    font-size: 10px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
  }

  .language {
    border-left: 1.5px solid #ffffff;
    font-family: "Roboto";
    font-size: 12px;
    font-weight: 500;
  }
`;

class Header extends Component {
  render() {
    return (
      <StyledHeader>
        <div className="title-page">
          <i className="fas fa-home" />
          <span className="title-page-text">Home</span>
        </div>
        <div className="customer-services">
          <i className="fas fa-file-alt" />
          Customer Surveys
        </div>
        <div className="message">
          <i className="fas fa-envelope">
            <span className="number-message">1</span>
          </i>
        </div>
        <div className="language">EN</div>
      </StyledHeader>
    );
  }
}

export default Header;
