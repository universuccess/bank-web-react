import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";

import Header from "./Header";
import Main from "./Main/Main";
import Footer from "./Footer";
import MessageBox from "./MessageBox";

const StyledRightContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background: linear-gradient(121deg, #00a1e4, #005d98) center center fixed;
  background-size: cover;
  flex-grow: 1;
  min-width: 1180px;

  & > .main {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
  }
`;

class RightContent extends Component {
  render() {
    return (
      <StyledRightContent>
        <Header />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <div className="main">
                <Main />
              </div>
            )}
          />
          <Route path="/chat" component={MessageBox} />
        </Switch>
        <Footer />
      </StyledRightContent>
    );
  }
}

export default RightContent;
