import React, { Component } from "react";
import styled from "styled-components";

import Staff from "./Staff";

import iconWee from "../../../img/group-12.png";
import iconD from "../../../img/group-5.png";

const StyledSendMoneyToFriend = styled.div`
  background-image: linear-gradient(
    37deg,
    #9000ff 0%,
    #8100e1 24%,
    #005cec,
    #005bec
  );

  & > .title-send-money {
    font-family: "UTM-Avo";
    font-size: 12px;
    padding-top: 30px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 80%;
    margin: 0 auto;
  }

  & > .title-send-money > img {
    width: 51px;
    height: 18px;
  }

  .money-send {
    height: 50px;
    width: 80%;
    display: flex;
    flex-direction: row;
    position: relative;
    margin: 45px auto 0 auto;
    border-radius: 5px;
  }

  .money-send > * {
    height: 100%;
    border: none;
    outline: none;
    color: #ffffff;
    background: #9000ff;
  }

  button.increase-money {
    border-radius: 50%;
    position: absolute;
    top: 0;
    left: 0;
    transform: translate(-50%, 120%);
  }

  button.increase-money,
  button.increase-money * {
    padding: 0;
    width: 14px;
    height: 14px;
  }

  .money-send > input {
    flex-grow: 1;
    width: 100%;
    font-family: "Lato";
    font-size: 24px;
    font-weight: bold;
    letter-spacing: 0.4px;
    padding: 10px;
    appearance: none;
    text-align: center;
  }

  button.btn-dong {
    color: #ffffff;
  }

  & .search-box-friend {
    width: 80%;
    margin: 26px auto 0 auto;
    border-bottom: 0.5px solid #ffffff;
  }

  & .search-box-friend > * {
    opacity: 0.5;
    color: #ffffff;
  }

  & .search-box-friend input {
    background: transparent;
    border: none;
    outline: none;
    padding: 10px;
    font-family: "UTM-Avo";
    font-size: 12px;
  }

  & .wee-staff {
    margin: 15.8px auto 45px 16px;
    display: flex;
    overflow: scroll;
  }

  & .wee-staff > * {
    flex-shrink: 0;
  }

  & .wee-staff::-webkit-scrollbar {
    display: none;
  }
`;

class SendMoneyToFriend extends Component {
  state = {
    users: [
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" },
      { img: "https://picsum.photos/200/300", name: "Thùy Dương" }
    ],
    number: ""
  };

  onScrollListen = e => {
    e.preventDefault();

    if (e.deltaY < 0) {
      e.currentTarget.scrollLeft -= 20;
    }
    if (e.deltaY > 0) {
      e.currentTarget.scrollLeft += 20;
    }
  };

  formatNumber = e => {
    this.setState({
      number: e.target.value.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    });
  };

  render() {
    return (
      <StyledSendMoneyToFriend>
        <div className="title-send-money">
          <div>DRAG MONEY TO SEND</div>
          <img src={iconWee} alt="icon-wee" />
        </div>
        <div className="money-send">
          <button className="increase-money">
            <i className="fas fa-plus-circle" />
          </button>
          <input
            type="text"
            value={this.state.number}
            onChange={this.formatNumber}
          />
          <button className="btn-dong">
            <img src={iconD} alt="icon-dong" />
          </button>
        </div>
        <div className="search-box-friend">
          <i className="fas fa-search" />
          <input type="text" placeholder="Search wee-name" />
        </div>
        <div className="wee-staff">
          {this.state.users.map((user, key) => (
            <Staff user={user} key={key} />
          ))}
        </div>
      </StyledSendMoneyToFriend>
    );
  }

  componentDidMount() {
    document
      .querySelector("div.wee-staff")
      .addEventListener("wheel", this.onScrollListen);
  }
}

export default SendMoneyToFriend;
