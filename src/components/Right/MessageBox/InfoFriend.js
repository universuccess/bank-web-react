import React, { Component } from "react";
import styled from "styled-components";

const StyledInfoFriend = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  text-align: center;
  font-family: "Roboto";

  .img-and-stt {
    position: relative;
    display: inline-block;
    width: 120px;
    height: 120px;
  }

  .img-and-stt > img {
    width: 120px;
    height: 120px;
  }

  .img-and-stt > img,
  .img-and-stt > span {
    border-radius: 50%;
  }

  .img-and-stt > span {
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    height: 32px;
    background: #34d859;
    border: 8px solid #ffffff;
  }

  .header-info > .name {
    font-size: 18px;
    font-weight: 500;
    color: #001654;
  }

  .header-info > .tag-friend {
    font-family: "Roboto";
    font-size: 14px;
    font-weight: 300;
    color: #b5b5b5;
  }

  & .field-info {
    margin: 21px auto 0 auto;
    width: 80%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  & .field-info > * {
    font-size: 14px;
    font-weight: 300;
  }

  & .field-info > .acc-title {
    color: #b5b5b5;
  }

  & .field-info > .acc-number {
    color: #001654;
  }

  & .field-info > .field-info-title {
    color: #001654;
  }

  & .filed-info > .see-all-btn {
    color: #f5581c;
  }
`;

class InfoFriend extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <StyledInfoFriend>
        <div className="header-info">
          <div className="img-and-stt">
            <img src={this.props.userInfo.img} alt="img" />
            <span className="status" />
          </div>
          <div className="name">{this.props.userInfo.name}</div>
          <div className="tag-friend">{this.props.userInfo.tag}</div>
        </div>

        <div className="field-info">
          <div className="acc-title">Acc No: </div>
          <div className="acc-number">{this.props.userInfo.id}</div>
        </div>

        <div className="field-info">
          <div className="field-info-title">Media(31)</div>
          <div className="see-all-btn">
            See all <i className="fas fa-angle-right" />
          </div>
        </div>

        <div className="field-info">
          <div className="field-info-title">Last transaction</div>
          <div className="see-all-btn">
            See all <i className="fas fa-angle-right" />
          </div>
        </div>
      </StyledInfoFriend>
    );
  }

  componentDidMount() {
    // console.log(this.props.userInfo);
  }
}

export default InfoFriend;
