import React, { Component } from "react";
import styled from "styled-components";

const StyledItemFriendChat = styled.div`
  border-radius: 5px;
  background-image: linear-gradient(76deg, #f98153, #f45112);
  width: 330px;
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-bottom: 15px;

  &:hover {
    cursor: pointer;
  }

  & .img-friend-status {
    position: relative;
  }

  & img {
    width: 46px;
    height: 46px;
    border-radius: 50%;
  }

  & .img-friend-status span {
    position: absolute;
    width: 16px;
    height: 16px;
    background: #34d859;
    border: 4px solid #f98153;
    border-radius: 50%;
    left: 0;
  }

  & .name-id-last-message {
    flex-basis: 51%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  & .name-id-last-message,
  & .menu-option {
    height: 46px;
  }

  & .name-id-last-message > *,
  & .last-online {
    font-family: "Roboto";
    color: #ffffff;
  }

  & .name-id-last-message > .name-friend {
    font-size: 16px;
    font-weight: 500;
  }

  & .name-id-last-message > .last-message {
    font-size: 12px;
    font-weight: 300;
  }

  .last-online {
    font-weight: 300;
    opacity: 0.5;
  }
`;

class ItemFriendChat extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <StyledItemFriendChat
        onClick={id => this.props.checkChangeFriendChat(this.props.user.id)}
      >
        <div className="img-friend-status">
          <img src={this.props.user.img} alt="img-friend" />
          <span className="status" />
        </div>

        <div className="name-id-last-message">
          <div className="name-friend">{this.props.user.name}</div>
          <div className="last-message">
            {this.props.user.lstChat[this.props.user.lstChat.length - 1].me}
          </div>
        </div>

        <div className="last-online">{this.props.user.lastOnline}</div>
      </StyledItemFriendChat>
    );
  }
}

export default ItemFriendChat;
