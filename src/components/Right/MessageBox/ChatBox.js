import React, { Component } from "react";
import styled from "styled-components";

const StyledChatBox = styled.div`
  min-width: 560px;
  position: relative;

  .header-chat-box {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    height: 90px;
  }

  .header-chat-box .img-friend-status {
    position: relative;
  }

  .header-chat-box img {
    width: 46px;
    height: 46px;
    border-radius: 50%;
  }

  .header-chat-box .img-friend-status span {
    position: absolute;
    width: 16px;
    height: 16px;
    background: #34d859;
    border: 4px solid #f98153;
    border-radius: 50%;
    left: 0;
  }

  .header-chat-box .name-tag-last-online {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    flex-basis: 60%;
  }

  .header-chat-box .name-tag-last-online,
  .header-chat-box .menu-option {
    height: 46px;
  }

  .header-chat-box .name-tag-last-online > * {
    font-family: "Roboto";
    color: #001654;
  }

  .header-chat-box .name-tag-last-online > .name-friend {
    font-size: 18px;
    font-weight: 500;
    line-height: 1.22;
  }

  .header-chat-box .name-tag-last-online > .tag-last-online {
    font-size: 12px;
    font-weight: 300;
    opacity: 0.5;
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .tag-last-online > * {
    margin-right: 10px;
  }

  .tag-last-online > span {
    width: 4px;
    height: 4px;
    opacity: 0.5;
    background-color: #001654;
    border-radius: 50%;
  }

  .chat-field {
    /* overflow: scroll; */

    /* position: relative; */
    width: 80%;
    margin: 0 auto;
    max-height: 600px;
    overflow: scroll;
  }

  .chat-field::-webkit-scrollbar {
    display: none;
  }

  .message-content {
    display: flex;
    flex-direction: column;
    font-family: "Roboto";
    font-weight: 300;
  }

  .message-content.right {
    margin-right: 19px;
  }

  .left {
    align-items: flex-start;
    text-align: left;
  }

  .right {
    align-items: flex-end;
    text-align: right;
  }

  .message {
    padding: 12px 20px;
    margin-top: 25px;
    border-radius: 12px;
  }

  .left > .message {
    background-color: #eff1f5;
    color: #001654;
  }

  .right > .message {
    background-color: #008bcc;
    color: #ffffff;
  }

  .time-chat {
    margin-top: 6px;
    line-height: 1.2;
    color: #001654;
    opacity: 0.5;
  }

  .input-field-chat {
    position: absolute;
    bottom: 0;
    width: 80%;
    margin: 12px auto;
    display: flex;
    flex-direction: row;
    height: 48px;
    border-radius: 24px;
    background: #eff1f5;
  }

  .input-field-chat button,
  .input-field-chat > input {
    border: none;
    outline: none;
    background: #eff1f5;
    font-size: 20px;
    color: #d0d4da;
  }

  .input-field-chat > .btn-attrack,
  .input-field-chat > .btn-react {
    margin-left: 20px;
    margin-right: 20px;
  }

  .input-field-chat > input {
    flex-grow: 1;
    font-family: "Roboto";
    font-weight: 300;
    font-size: 16px;
    color: #181c2f;
    opacity: 0.3;
    line-height: 1.38;
  }

  .input-field-chat > .btn-send {
    width: 52px;
    height: 52px;
    border-radius: 50%;
    background-image: linear-gradient(28deg, #f98153, #f45112);
  }

  .input-field-chat > .btn-send i {
    color: #ffffff;
  }
`;

class ChatBox extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.inputRef = React.createRef();

    this.fiedChatRef = React.createRef();
  }

  sendMsg = e => {
    e.preventDefault();

    const chat = this.inputRef.current.value;
    const id = this.props.userInfo.id;

    this.inputRef.current.value = "";

    // console.log(chat);
    console.log(this.fiedChatRef);
    this.fiedChatRef.current.scrollTo(0, 0);

    this.props.saveChatMsgToDatabase(chat, id);
  };

  render() {
    return (
      <StyledChatBox>
        <div className="header-chat-box">
          <div className="img-friend-status">
            <img
              src="http://www.khamphathegioi.org/Content/img/tintuc/images/infact/7-ky-quan-thien-nhien-the-gioi-3.jpg"
              alt="img-friend"
            />
            <span className="status" />
          </div>

          <div className="name-tag-last-online">
            <div className="name-friend">{this.props.userInfo.name}</div>
            <div className="tag-last-online">
              <div className="tag">{this.props.userInfo.tag}</div>
              <span />
              <div className="last-online">Last seen 3 hours ago</div>
            </div>
          </div>

          <div className="menu-option">
            <i className="fas fa-ellipsis-h" />
          </div>
        </div>

        <div className="chat-field">
          <div className="messsage-field-chat" ref={this.fiedChatRef}>
            {this.props.userInfo.lstChat.map((chat, key) =>
              Boolean(chat.me) ? (
                <div className="message-content right" key={key}>
                  <div className="message">{chat.me}</div>
                  <div className="time-chat">14.26 PM</div>
                </div>
              ) : (
                <div className="message-content left" key={key}>
                  <div className="message">{chat.friend}</div>
                  <div className="time-chat">14.26 PM</div>
                </div>
              )
            )}
          </div>
          <div className="input-field-chat">
            <button className="btn-attrack">
              <i className="fas fa-paperclip" />
            </button>
            <input
              type="text"
              placeholder="Type something"
              autoFocus="autofocus"
              ref={this.inputRef}
            />
            <button className="btn-react">
              <i className="far fa-smile" />
            </button>
            <button
              className="btn-send"
              ref={this.btnSubmitRef}
              onClick={this.sendMsg}
            >
              <i className="fas fa-paper-plane" />
            </button>
          </div>
        </div>
      </StyledChatBox>
    );
  }

  componentDidMount() {
    console.log(this.props.userInfo.chat);
  }
}

export default ChatBox;
