import React, { Component } from "react";
import styled from "styled-components";

const StyledStaff = styled.div`
  width: 80px;
  height: 80px;
  border-radius: 10px;
  background-color: rgba(255, 255, 255, 0.15);
  margin-right: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  & img {
    width: 36px;
    height: 36px;
    border-radius: 50%;
  }

  & .name-staff {
    font-size: 12px;
    color: #ffffff;
    margin-top: 5px;
  }
`;

class Staff extends Component {
  render() {
    return (
      <StyledStaff>
        <img src={this.props.user.img} alt="img-staff" />
        <div className="name-staff">{this.props.user.name}</div>
      </StyledStaff>
    );
  }
}

export default Staff;
