import React, { Component } from "react";
import styled from "styled-components";

import ItemFriendChat from "./ItemFriendChat";

import addFriendIcon from "../../../img/group-7.png";

const StyledSelectionChatBox = styled.div`
  min-width: 360px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  & > .search-box,
  & > .tool-box {
    min-width: 320px;
    font-family: "Roboto";
    font-weight: 300;
  }

  & > .search-box {
    margin-top: 17px;
    height: 40px;
    font-size: 14px;
  }

  & > .search-box > * {
    display: inline-block;
    height: 100%;
    border: none;
    outline: none;
    background: #eff1f5;
    padding-left: 16px;
    color: #001654;
  }

  & > .search-box > button {
    width: 20%;
    border-top-left-radius: 24px;
    border-bottom-left-radius: 24px;
  }

  & > .search-box > input {
    width: 80%;
    border-top-right-radius: 24px;
    border-bottom-right-radius: 24px;
  }

  & > .search-box,
  & > .tool-box {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }

  & > .tool-box {
    margin-top: 18px;
    font-size: 12px;
    line-height: 1.17;
  }

  & > .tool-box > div {
    flex-basis: 41%;
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  & > .tool-box :first-child {
    justify-content: flex-start;
  }

  & > .tool-box :last-child {
    justify-content: flex-end;
  }

  & > .tool-box .sort :first-child {
    color: #181c2f;
    margin-right: 11px;
  }

  & > .tool-box .sort :last-child {
    font-weight: 400;
    color: #001654;
  }

  & > .tool-box > .create-message-box :first-child {
    color: #001654;
    margin-right: 10px;
  }

  & > .tool-box > .create-message-box > img {
    width: 30px;
    height: 30px;
  }

  & .lst-fr-chat {
    min-width: 330px;
    max-height: 450px;
    margin-top: 27px;
    overflow: scroll;
  }

  & .lst-fr-chat::-webkit-scrollbar {
    width: 4px;
    color: #f98153;
  }
`;

class SelectionChatBox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <StyledSelectionChatBox>
        <div className="search-box">
          <button>
            <i className="fas fa-search" />
          </button>
          <input type="text" placeholder="Search" />
        </div>

        <div className="tool-box">
          <div className="sort">
            <div>Sort by:</div>
            <div>
              Latest <i className="fab fa-vuejs" />
            </div>
          </div>

          <div className="create-message-box">
            <div>New Message</div>
            <img src={addFriendIcon} alt="" />
          </div>
        </div>

        <div className="lst-fr-chat">
          {this.props.users.map((user, key) => (
            <ItemFriendChat
              key={key}
              user={user}
              checkChangeFriendChat={this.props.checkChangeFriendChat}
            />
          ))}
        </div>
      </StyledSelectionChatBox>
    );
  }
}

export default SelectionChatBox;
