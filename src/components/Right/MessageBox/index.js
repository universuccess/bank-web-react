import React, { Component } from "react";
import styled from "styled-components";

import SelectionChatBox from "./SelectionChatBox";
import ChatBox from "./ChatBox";
import SendMoneyToFriend from "./SendMoneyToFriend";
import InfoFriend from "./InfoFriend";

const StyledMessageBox = styled.div`
  display: flex;
  flex-direction: row;
  background: #ffffff;
  height: 100%;

  .right-box-chat {
    min-width: 260px;
  }
`;

class MessageBox extends Component {
  state = {
    users: [
      {
        name: "user 1",
        tag: "user",
        img: "https://www.w3schools.com/w3css/img_lights.jpg",
        username: "user1",
        id: "20005154",
        lstChat: [{ me: "aaaaa" }, { friend: "bbbbb" }, { me: "ccccc" }],
        lastOnline: "3 mins"
      },
      {
        name: "user 2",
        tag: "user",
        img:
          "https://cdn.pixabay.com/photo/2017/07/17/09/49/space-2511845_960_720.jpg",
        username: "user2",
        id: "313546879815",
        lstChat: [{ me: "1" }, { friend: "2" }, { me: "3" }],
        lastOnline: "3 mins"
      },
      {
        name: "user 3",
        tag: "user",
        img:
          "http://www.khamphathegioi.org/Content/img/tintuc/images/infact/7-ky-quan-thien-nhien-the-gioi-3.jpg",
        username: "user3",
        id: "24679851",
        lstChat: [{ me: "dddd" }, { friend: "jdhgfff" }, { me: "dddd" }],
        lastOnline: "3 mins"
      },
      {
        name: "user 4",
        tag: "user",
        img:
          "http://tule.lechamp.vn/uploads/posts/5b47320bc632dpexels-photo-531880.jpeg",
        username: "user4",
        id: "687984561",
        lstChat: [
          { me: "00987" },
          { friend: "abdsjsakdnas" },
          { me: "wqoeiuoqwi" }
        ],
        lastOnline: "3 mins"
      }
    ],
    selectIdUser: "20005154",
    scrollToBottom: ""
  };

  checkChangeFriendChat = id => {
    this.setState({ selectIdUser: id });
  };

  saveChatMsgToDatabase = (chat, id, ref) => {
    let newState = JSON.parse(JSON.stringify(this.state));

    newState.users.forEach(user => {
      if (user.id === id) {
        user.lstChat.push({ me: chat });
      }
    });

    this.setState(newState);
    this.setState({ scrollToBottom: ref });
  };

  render() {
    return (
      <StyledMessageBox>
        <SelectionChatBox
          users={this.state.users}
          selectIdUser={this.state.selectIdUser}
          checkChangeFriendChat={this.checkChangeFriendChat}
        />
        <ChatBox
          userInfo={
            this.state.users.filter(
              user => user.id === this.state.selectIdUser
            )[0]
          }
          saveChatMsgToDatabase={this.saveChatMsgToDatabase}
        />
        <div className="right-box-chat">
          <SendMoneyToFriend getData={this.getData} />

          <InfoFriend
            userInfo={
              this.state.users.filter(
                user => user.id === this.state.selectIdUser
              )[0]
            }
          />
        </div>
      </StyledMessageBox>
    );
  }
  componentDidUpdate() {
    const objDiv = document.querySelector(".messsage-field-chat");
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  componentDidMount() {
    console.log("selectIdUser:" + this.state.selectIdUser);
  }
}

export default MessageBox;
