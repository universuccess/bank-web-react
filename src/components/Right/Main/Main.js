import React, { Component } from "react";
import styled from "styled-components";
import TableMain from "./TableMain";
import LeftMain from "./LeftMain/LeftMain";
import RightMain from "./RightMain/RightMain";

const StyledContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;

  & > .info-and-option {
    margin-top: 40px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  & > .info-and-option > * {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
`;

export default class Main extends Component {
  render() {
    return (
      <StyledContent>
        <div className="info-and-option">
          <LeftMain />
          <RightMain />
        </div>
        <TableMain />
      </StyledContent>
    );
  }
}
