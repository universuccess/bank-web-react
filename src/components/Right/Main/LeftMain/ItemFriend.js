import React, { Component } from "react";
import styled from "styled-components";

const StyledItemFriend = styled.div`
  & > img {
    width: 40.5px;
    height: 40.5px;
    border-radius: 50%;
  }

  & > div {
    color: #001654;
    font-family: "UTM-Avo";
    font-size: 12px;
  }
`;

class ItemFriend extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <StyledItemFriend>
        <img src={this.props.img} alt="img-friend" />
        <div>{this.props.name}</div>
      </StyledItemFriend>
    );
  }
}

export default ItemFriend;
