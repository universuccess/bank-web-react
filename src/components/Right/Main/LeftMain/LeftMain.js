import React, { Component } from "react";
import styled from "styled-components";
import MoneyShow from "./MoneyShow";
import ListFriend from "./ListFriend";

const StyledLeftMain = styled.div`
  width: 726px;

  & > .moneyShow {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  & > img {
    margin-top: 20px;
    width: 726px;
    height: 170px;
  }
`;

class LeftMain extends Component {
  render() {
    return (
      <StyledLeftMain>
        <div className="moneyShow">
          <MoneyShow title="CURRENT ACCOUNT" numbMoney="12,864,000.00" />
          <MoneyShow title="TERM DEPOSITS ACCOUNT" numbMoney="200,000,000.00" />
        </div>
        {/* <Advert /> */}
        <img src="https://picsum.photos/200/700" alt="advert-img" />
        <ListFriend />
      </StyledLeftMain>
    );
  }
}

export default LeftMain;
