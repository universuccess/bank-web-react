import React, { Component } from "react";
import styled from "styled-components";

import ItemFriend from "./ItemFriend";

import addFriendIcon from "../../../../img/group-7.png";

const StyledListFriend = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 20px;

  & > .wrap-add-fr,
  & > .list-friend > * {
    width: 90px;
    height: 90px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    margin-right: 20px;
    background: #ffffff;
    border-radius: 10px;
  }

  & > .wrap-add-fr > img {
    width: 36px;
    height: 36px;
  }

  & > .list-friend {
    display: -webkit-box;
    overflow: scroll;
    width: 526px;
    flex-grow: 1;
  }

  & > .list-friend::-webkit-scrollbar {
    display: none;
  }
`;

class ListFriend extends Component {
  state = {
    listFriend: [
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"],
      ["Thùy Dương", "https://picsum.photos/200/300"]
    ]
  };

  onScrollListen = e => {
    e.preventDefault();

    if (e.deltaY < 0) {
      e.currentTarget.scrollLeft -= 20;
    }
    if (e.deltaY > 0) {
      e.currentTarget.scrollLeft += 20;
    }
  };

  render() {
    return (
      <StyledListFriend>
        <div className="wrap-add-fr">
          <img src={addFriendIcon} alt="icon-add-friend" />
        </div>
        <div className="list-friend">
          {this.state.listFriend.map((item, key) => (
            <ItemFriend key={key} img={item[1]} name={item[0]} />
          ))}
        </div>
      </StyledListFriend>
    );
  }

  componentDidMount() {
    document
      .querySelector("div.list-friend")
      .addEventListener("wheel", this.onScrollListen);
  }
}

export default ListFriend;
