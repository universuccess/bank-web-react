import React, { Component } from "react";
import styled from "styled-components";

import vndImg from "../../../../img/combined-shape.png";
import eyeImg from "../../../../img/ic-invisible-copy.png";

const StyledMoneyShow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  min-width: 353px;
  height: 90px;

  opacity: 0.95;
  border-radius: 5px;
  box-shadow: 0 6px 10px 0 rgba(169, 71, 0, 0.25);
  background-image: linear-gradient(65deg, #f98153, #f45112);

  & > div {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: auto 20px;
  }

  & > .title-line {
    align-items: center;
  }

  & > .title-line :first-child {
    font-family: "Roboto";
    font-size: 12px;
    font-weight: 500;
    color: #ffffff;
  }

  & > .money-line {
    align-items: center;
  }

  & > .title-line > img {
    width: 20px;
    height: 20px;
  }

  & > .money-line > img {
    width: 46px;
    height: 21px;
  }

  & > .money-line > div {
    font-family: "Lato";
    font-size: 28px;
    letter-spacing: 0.3px;
    color: #ffffff;
  }
`;

class MoneyShow extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <StyledMoneyShow>
        <div className="title-line">
          <div>{this.props.title}</div>
          <img src={eyeImg} alt="icon-eye" />
        </div>
        <div className="money-line">
          <img src={vndImg} alt="icon-vnd" />
          <div>{this.props.numbMoney}</div>
        </div>
      </StyledMoneyShow>
    );
  }
}

export default MoneyShow;
