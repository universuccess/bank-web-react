import React, { Component } from "react";
import styled from "styled-components";

const StyledBill = styled.div`
  height: 170px;
  border-radius: 5px;
  background-image: linear-gradient(64deg, #00cfff, #005cec, #005bec);
  padding-left: 23px;
  font-family: "Roboto";
  color: #ffffff;
  position: relative;
  z-index: 0;

  &::before {
    content: "";
    position: absolute;
    background-image: url("https://picsum.photos/200/300");
    opacity: 0.3;
    background-size: cover;
    display: block;
    top: 0;
    left: 0;
    z-index: -1;
    width: 100%;
    height: 100%;
  }

  &,
  & > .info-money {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
  }

  & > .title-bill {
    font-size: 12px;
    font-weight: 500;
    margin-left: 2px;
  }

  & > .info-money > .data,
  & > .info-money > .detail {
    font-size: 16px;
    font-weight: 300;
  }

  & > .info-money > .vnd {
    font-family: "Lato";
    font-size: 28px;
    font-weight: bold;
  }

  & > .info-money > .detail {
    font-style: italic;
  }
`;

export default class Bill extends Component {
  render() {
    return (
      <StyledBill>
        <div className="title-bill">NEXT RECURRING BILL PAYMENT</div>
        <div className="info-money">
          <div className="date">15 October, 2018</div>
          <div className="vnd">220,000 VND</div>
          <div className="detail">Water bill monthly</div>
        </div>
      </StyledBill>
    );
  }
}
