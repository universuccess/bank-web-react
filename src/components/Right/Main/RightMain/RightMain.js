import React, { Component } from "react";
import styled from "styled-components";
import Bill from "./Bill";
import BoxHelpChat from "./BoxHelpChat";

const StyledRightMain = styled.div`
  width: 353px;
  margin-left: 21px;
`;

class RightMain extends Component {
  render() {
    return (
      <StyledRightMain>
        <Bill />
        <BoxHelpChat />
      </StyledRightMain>
    );
  }
}

export default RightMain;
