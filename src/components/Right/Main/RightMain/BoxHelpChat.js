import React, { Component } from "react";
import styled from "styled-components";

const StyleBoxHelpChat = styled.div`
  height: 200px;
  border-radius: 5px;
  background-image: linear-gradient(to top, #000f2f, #00385c);
  color: #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding-left: 20px;

  & > .title-box-chat {
    font-family: "Roboto";
    font-size: 28px;
    margin-left: 7px;
  }

  & .title-box {
    font-weight: 500;
  }

  & .question-box {
    font-weight: 300;
  }

  & > .input-form {
    width: 313px;
    height: 45px;
    display: flex;
    flex-direction: row;
  }

  & > .input-form > * {
    height: 100%;
    border: none;
    background-color: #1a304e;
    outline: none;
    color: #ffffff;
    font-size: 18px;
    font-weight: 300;
  }

  & > .input-form > input {
    padding-left: 10%;
    width: 80%;
    border-top-left-radius: 22.5px;
    border-bottom-left-radius: 22.5px;
  }

  & > .input-form > button {
    flex-grow: 1;
    border-top-right-radius: 22.5px;
    border-bottom-right-radius: 22.5px;
  }
`;

class BoxHelpChat extends Component {
  render() {
    return (
      <StyleBoxHelpChat>
        <div className="title-box-chat">
          <div className="title-box">Hello</div>
          <div className="question-box">May I help you</div>
        </div>
        <div className="input-form">
          <input type="text" placeholder="Answer here" />
          <button>
            <i className="fas fa-arrow-right" />
          </button>
        </div>
      </StyleBoxHelpChat>
    );
  }
}

export default BoxHelpChat;
