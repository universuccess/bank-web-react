import React, { Component } from "react";
import styled from "styled-components";

const StyledTableMain = styled.div`
  width: 1100px;
  margin-top: 21px;

  & table {
    width: 100%;
    table-layout: fixed;
  }
  & .tbl-header {
    height: 40px;
    background-color: #1aa4e5;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    font-family: "Roboto";
  }
  & .tbl-content {
    height: 220px;
    overflow-x: auto;
    border: 1px solid rgba(255, 255, 255, 0.3);
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
  }

  & .tbl-content::-webkit-scrollbar {
    width: 4px;
    opacity: 0.5;
    color: #ffffff;
  }
  & .tbl-content::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  }
  & .tbl-content::-webkit-scrollbar-thumb {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  }

  & tr {
    height: 40px;
  }

  & tbody tr {
    background: #0070a6;
  }

  & th {
    text-align: center;
    font-weight: 500;
    font-size: 12px;
    color: #fff;
    text-transform: uppercase;
  }
  & td {
    text-align: center;
    vertical-align: middle;
    font-weight: 300;
    font-size: 14px;
    line-height: 2.89;
    color: #fff;
    border-bottom: solid 1px rgba(255, 255, 255, 0.1);
  }
`;

class TableMain extends Component {
  state = {
    transactions: [
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      },
      {
        date: "30 Sep 2018",
        type: "Topup",
        details: "Phone number: 0987654321",
        deposits: "10,0000,000",
        withDrawals: "200,000",
        balance: "12,964,000"
      }
    ]
  };
  render() {
    return (
      <StyledTableMain>
        <div className="tbl-header">
          <table cellPadding="0" cellSpacing="0" border="0">
            <thead>
              <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Details</th>
                <th>Deposits</th>
                <th>Withdrawals</th>
                <th>Balance</th>
              </tr>
            </thead>
          </table>
        </div>
        <div className="tbl-content">
          <table cellPadding="0" cellSpacing="0" border="0">
            <tbody>
              {this.state.transactions.map((trans, key) => (
                <tr key={key}>
                  <td>{trans.date}</td>
                  <td>{trans.type}</td>
                  <td>{trans.details}</td>
                  <td>{trans.deposits}</td>
                  <td>{trans.withDrawals}</td>
                  <td>{trans.balance}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </StyledTableMain>
    );
  }
}

export default TableMain;
